package gencana.com.weatherapp.common.utils

import gencana.com.weatherapp.BuildConfig
import gencana.com.weatherapp.common.constant.ApiConstants
import org.junit.After
import org.junit.Before
import org.junit.Test

import org.junit.Assert.*

/**
 * Created by Gen Cana on 05/08/2018
 */
class HttpUtilKtTest {

    @Before
    fun setUp() {
    }

    @After
    fun tearDown() {
    }

    @Test
    fun buildGetUri() {
        val map: LinkedHashMap<String, String> = LinkedHashMap()
        map.put(ApiConstants.PARAM_API_KEY, BuildConfig.WEATHERBIT_API_KEY)
        map.put(ApiConstants.PARAM_LATITUDE, "14.5889307")
        map.put(ApiConstants.PARAM_LONGITUDE, "121.0933923")

        assertEquals("Parsed URL is incorrect!!!",
                String.format("%s%s%s=%s&%s=%s&%s=%s", BuildConfig.WEATHERBIT_BASE_URL, ApiConstants.ENDPOINT_CURRENT_WEATHER,
                        ApiConstants.PARAM_API_KEY, BuildConfig.WEATHERBIT_API_KEY,
                        ApiConstants.PARAM_LATITUDE, "14.5889307",
                        ApiConstants.PARAM_LONGITUDE, "121.0933923"),
                buildGetUri(ApiConstants.ENDPOINT_CURRENT_WEATHER, map, BuildConfig.WEATHERBIT_BASE_URL))
    }
}