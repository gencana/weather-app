package gencana.com.weatherapp.domain.model

import org.json.JSONObject
import org.junit.After
import org.junit.Before
import org.junit.Test

import org.junit.Assert.*

/**
 * Created by Gen Cana on 05/08/2018
 */
class WeatherModelTest1 {

    @Before
    fun setUp() {
    }

    @After
    fun tearDown() {
    }

    @Test
    fun parseFromJson() {
        var weatherModel = generateDummyWeatherModel()

        assertFalse("Incorrect parsing for Incomplete Json",
                WeatherModel().parseFromJson(generateIncompleteJson().toString())?.equals(weatherModel)!!)

        assertTrue("Incorrect parsing for Incomplete Valid Json",
                WeatherModel().parseFromJson(generateIncompleteJson().toString())?.equals(generateIncompleteModel())!!)

        assertTrue("Incorrect parsing for Complete Valid Json",
                WeatherModel().parseFromJson(generateSampleJson().toString())?.equals(weatherModel)!!)

        assertFalse("Incorrect parsing for Incorect Json",
                WeatherModel().parseFromJson(generateIncorrectJson().toString())?.equals(weatherModel)!!)

        assertFalse("Incorrect equal() for Null Model",
                WeatherModel().parseFromJson(generateSampleJson().toString())?.equals(null)!!)
    }

    private fun generateDummyWeatherModel(): WeatherModel{
        val weather = WeatherModel()
        weather.windCdir = "N"
        weather.dayPart = "d"
        weather.pressure = 1007
        weather.longitude = 121.09
        weather.humidity = 87
        weather.timezone = "Asia/Manila"
        weather.observationTime = "2018-08-05 03:33"
        weather.countryCode = "PH"
        weather.clouds = 0
        weather.visbility = 10
        weather.windSpeed = 1.34
        weather.stateCode = "NCR"
        weather.latitude = 14.59
        weather.windCdirFull = "north"
        weather.seaLevelPressure = 1010
        weather.dateTime = "2018-08-05:03"
        weather.timestamp = 1533439980
        weather.station = "E0758"
        weather.hourAngle = -12.9
        weather.dewPoint = 25.4
        weather.uv = 11.0876
        weather.dni = 780.91
        weather.windDir = 0
        weather.elevationAngle = 74.9751
        weather.ghi = 932.676
        weather.dhi = 178.447
        weather.cityName = "Pasig City"
        weather.apparentTemperature = 32.6
        weather.sunset = "10:23"
        weather.temp = 27.8
        weather.sunrise = "21:39"

        weather.weatherIcon = "c01d"
        weather.weatherCode = "800"
        weather.weatherDescription = "Clear sky"

        return weather
    }

    private fun generateIncorrectJson(): JSONObject {
        return JSONObject("{phonetype:N95,cat:W}")
    }

    private fun generateIncompleteJson(): JSONObject {
        val json = JSONObject()
        json.put("wind_cdir", "N")
        json.put("rh", 87)
        return json
    }

    private fun generateIncompleteModel(): WeatherModel{
        val weatherModel = WeatherModel()
        weatherModel.windCdir = "N"
        weatherModel.humidity = 87

        return weatherModel
    }

    private fun generateSampleJson(): JSONObject {
        return JSONObject(
                " {\n" +
                        "      \"wind_cdir\": \"N\",\n" +
                        "      \"rh\": 87,\n" +
                        "      \"pod\": \"d\",\n" +
                        "      \"lon\": 121.09,\n" +
                        "      \"pres\": 1007,\n" +
                        "      \"timezone\": \"Asia/Manila\",\n" +
                        "      \"ob_time\": \"2018-08-05 03:33\",\n" +
                        "      \"country_code\": \"PH\",\n" +
                        "      \"clouds\": 0,\n" +
                        "      \"vis\": 10,\n" +
                        "      \"state_code\": \"NCR\",\n" +
                        "      \"wind_spd\": 1.34,\n" +
                        "      \"lat\": 14.59,\n" +
                        "      \"wind_cdir_full\": \"north\",\n" +
                        "      \"slp\": 1010,\n" +
                        "      \"datetime\": \"2018-08-05:03\",\n" +
                        "      \"ts\": 1533439980,\n" +
                        "      \"station\": \"E0758\",\n" +
                        "      \"h_angle\": -12.9,\n" +
                        "      \"dewpt\": 25.4,\n" +
                        "      \"uv\": 11.0876,\n" +
                        "      \"dni\": 780.91,\n" +
                        "      \"wind_dir\": 0,\n" +
                        "      \"elev_angle\": 74.9751,\n" +
                        "      \"ghi\": 932.676,\n" +
                        "      \"dhi\": 178.447,\n" +
                        "      \"precip\": null,\n" +
                        "      \"city_name\": \"Pasig City\",\n" +
                        "      \"weather\": {\n" +
                        "        \"icon\": \"c01d\",\n" +
                        "        \"code\": \"800\",\n" +
                        "        \"description\": \"Clear sky\"\n" +
                        "      },\n" +
                        "      \"sunset\": \"10:23\",\n" +
                        "      \"temp\": 27.8,\n" +
                        "      \"sunrise\": \"21:39\",\n" +
                        "      \"app_temp\": 32.6\n" +
                        "    }"
        )
    }
}