package gencana.com.weatherapp.view.main

import android.support.test.espresso.Espresso.onData
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.intent.rule.IntentsTestRule
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.rule.GrantPermissionRule
import android.support.test.runner.AndroidJUnit4
import gencana.com.weatherapp.R
import gencana.com.weatherapp.view.changelocation.ChangeLocationActivity
import gencana.com.weatherapp.view.details.DetailsActivity
import org.junit.After
import org.junit.Before
import org.junit.Rule

import org.junit.Test
import org.junit.runner.RunWith
import android.support.test.espresso.matcher.ViewMatchers.isDisplayed
import android.support.test.espresso.intent.Intents.*
import android.support.test.espresso.intent.matcher.IntentMatchers.*
import org.hamcrest.Matchers.*


/**
 * Created by Gen Cana on 27/08/2018
 */
@RunWith(AndroidJUnit4::class)
class MainActivityTest {


    @get: Rule var permissionTestRule = GrantPermissionRule.grant(android.Manifest.permission.ACCESS_FINE_LOCATION)
    @get: Rule var activityTestRule = IntentsTestRule(MainActivity::class.java)

    @Before
    fun setUp() {

    }

    @Test
    fun onChangeLocationMenuClicked(){
        onView(withId(R.id.action_change_location))
                .perform(click())
        intended(hasComponent(ChangeLocationActivity::class.java.name))
    }

    @Test
    fun onDayItemClicked(){
        onData(anything())
                .inAdapterView(withId(R.id.list_view))
                .atPosition(10)
                .perform(click())
        intended(hasComponent(DetailsActivity::class.java.name))
    }

    @Test
    fun onResponseFailed(){
        activityTestRule.activity.runOnUiThread {
            activityTestRule.activity.onResponseFailed(null)
        }
        onView(withId(R.id.view_error))
                .check(matches(isDisplayed()))

    }

    @After
    fun tearDown() {
    }
}