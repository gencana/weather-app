package gencana.com.weatherapp.common.application

import android.app.Application
import gencana.com.weatherapp.data.database.DaoHelper

/**
 * Created by Gen Cana on 01/09/2018
 */

class WeatherApplication: Application(){

    override fun onCreate() {
        super.onCreate()

        //init database
        DaoHelper.init(this)
    }
}