package gencana.com.weatherapp.common.base

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import gencana.com.weatherapp.common.extensions.visible
import kotlinx.android.synthetic.main.view_progress_bar.*
import kotlin.reflect.KClass
import android.content.Intent
import android.view.MenuItem


/**
 * Created by Gen Cana on 05/08/2018
 */

abstract class BaseActivity<VM: BaseViewModel<T>, T>: AppCompatActivity() {

    protected var viewModel: VM? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayout())
        if (getViewModel() != null) {
            viewModel = getViewModel()?.java?.newInstance()
            observeResponse()
        }
    }

    protected fun showBackButton(){
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun observeResponse() {
        viewModel?.responseLiveData?.observe(
                this,
                Observer { response ->
                    response?.let {
                        onResponseSuccess(it)
                    }
                }
        )

        viewModel?.errorLiveData?.observe(
                this,
                Observer { response ->
                    response?.let {
                        onResponseFailed(it)
                    }
                }
        )

        viewModel?.loadingLiveData?.observe(
                this,
                Observer { response ->
                    response?.let {
                        showLoading(it)
                    }
                }
        )

        viewModel?.showErrorViewLiveData?.observe(
                this,
                Observer { response ->
                    response?.let {
                        if (it){
                            showErrorView(it)
                        }
                    }
                }
        )
    }

    abstract fun onResponseFailed(message: String?)

    abstract fun onResponseSuccess(data: T)

    abstract fun getLayout(): Int

    abstract fun getViewModel(): KClass<VM>?

    open fun showErrorView(show: Boolean = true){

    }

    open fun showLoading(show: Boolean) {
        layout_progress?.visible(show)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

}