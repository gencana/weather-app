package gencana.com.weatherapp.common.base

import android.arch.lifecycle.MutableLiveData


/**
 * Created by Gen Cana on 05/08/2018
 */
abstract class BaseViewModel<T>{

    var loadingLiveData: MutableLiveData<Boolean> = MutableLiveData()

    var responseLiveData: MutableLiveData<T> = MutableLiveData()

    var errorLiveData: MutableLiveData<String> = MutableLiveData()

    var showErrorViewLiveData: MutableLiveData<Boolean> = MutableLiveData()

    abstract fun execute()

}