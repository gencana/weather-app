package gencana.com.weatherapp.common.constant

import android.support.annotation.StringDef

/**
 * Created by Gen Cana on 04/08/2018
 */

@Retention(AnnotationRetention.SOURCE)
@StringDef(ApiConstants.ENDPOINT_CURRENT_WEATHER)
annotation class ApiEndpoint

@Retention(AnnotationRetention.SOURCE)
@StringDef(ApiConstants.PARAM_LATITUDE, ApiConstants.PARAM_LONGITUDE, ApiConstants.PARAM_API_KEY)
annotation class ApiParameter

object ApiConstants{

    @Retention(AnnotationRetention.SOURCE)
    @StringDef(ApiConstants.ERROR_CODE_NETWORK, ApiConstants.ERROR_CODE_UNKNOWN)
    annotation class ApiError

    const val ENDPOINT_CURRENT_WEATHER = "current?"
    const val ENDPOINT_FORECAST_HOURLY = "forecast/hourly?"
    const val ENDPOINT_FORECAST_DAILY = "forecast/daily?"

    const val PARAM_LATITUDE = "lat"
    const val PARAM_LONGITUDE = "lon"
    const val PARAM_API_KEY = "key"
    const val PARAM_DAYS = "days"
    const val PARAM_CITY = "city"

    const val ERROR_UNKNOWN = "Ooops! Unknown error has occured. Please try again."
    const val ERROR_NETWORK = "Network connection failed. Please try again."
    const val ERROR_LOCATION = "Unable to obtain data. Please check the city and try again."

    const val ERROR_CODE_NETWORK = "1"
    const val ERROR_CODE_UNKNOWN = "2"
    const val ERROR_CODE_NO_CONTENT = 204

    const val MAX_DAYS = 10
}