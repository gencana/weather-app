package gencana.com.weatherapp.common.constant

/**
 * Created by Gen Cana on 07/08/2018
 */

object Constant{

    const val LABEL_HUMIDITY = "Humidity"
    const val LABEL_SUNRISE = "Sunrise"
    const val LABEL_SUNSET = "Sunset"
    const val LABEL_CHANCE_OF_RAIN = "Chance of Rain"
    const val LABEL_WIND = "Wind"
    const val LABEL_APPARENT_TEMP = "Feels Like"
    const val LABEL_UV = "UV Index"
    const val LABEL_PRESSURE = "Pressure"
    const val LABEL_CLOUDS_LOW = "Clouds Low"
    const val LABEL_CLOUDS_HI = "Clouds Hi"
    const val LABEL_MOONSET = "Moonset"
    const val LABEL_MOONRISE = "Moonrise"
    const val LABEL_SNOW= "Snow"
    const val LABEL_COUNTRY= "Country Code"
    const val LABEL_STATE= "State Code"

    const val PRESSURE_SYMBOL = "hPa"
    const val WIND_SPEED_SYMBOL = "m/s"
    const val DEGREE_SYMBOL = "°"
    const val PERCENT_SYMBOL = "%"
    const val SERVER_TIMEZONE = "UTC"
    const val WIND_DIRECTION = "Wind Direction"
    const val MILLIMETER = "mm"

    const val VIEW_CURRENT_WEATHER = 0
    const val VIEW_DAILY_WEATHER = 2
    const val VIEW_HOURLY_WEATHER = 1
    const val VIEW_EXTENDED_WEATHER = 3
    const val VIEW_DIVIDER = 4
    const val VIEW_DAILY_HEADER_WEATHER = 5

    const val VIEW_TYPE_COUNT = 6

    const val KEY_DATA = "KEY_DATA"
    const val KEY_HOURLY_DATA = "KEY_HOURLY_DATA"
    const val KEY_DAILY_DATA = "KEY_DAILY_DATA"
    const val KEY_CITY = "KEY_CITY"

    const val JSON_FILE_CITY = "city.json"
    const val MAX_HOUR_DATA_SIZE = 24
}