package gencana.com.weatherapp.common.constant

/**
 * Created by Gen Cana on 01/09/2018
 */
object DatabaseConstant {

    const val DB_NAME = "app_database"
    const val DB_VERSION = 1

    const val TBL_CITY = "tbl_city"
    const val COLUMN_CITY = "city_name"
}