package gencana.com.weatherapp.common.extensions

import android.content.Context
import android.support.annotation.LayoutRes
import android.support.annotation.StringRes
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast

/**
 * Created by Gen Cana on 05/08/2018
 */

fun Context.getDrawableResourceByName(imageName: String?): Int{
    if (imageName == null) return 0

    return resources.getIdentifier(imageName, "drawable", packageName)
}

fun Context.inflateLayoutView(@LayoutRes layoutView: Int, viewGroup: ViewGroup?, attachToRoot: Boolean = true): View {
    return LayoutInflater.from(this).inflate(layoutView, viewGroup, attachToRoot)
}

fun Context.inflateLayoutItemView(@LayoutRes layoutView: Int, viewGroup: ViewGroup?): View {
    return LayoutInflater.from(this).inflate(layoutView, viewGroup)
}

fun Context.toast(message: String){
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()

}

fun Context.toast(@StringRes message: Int){
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()

}