package gencana.com.weatherapp.common.extensions

import android.view.KeyEvent
import android.view.inputmethod.EditorInfo
import android.widget.Button
import android.widget.EditText

/**
 * Created by Gen Cana on 03/09/2018
 */

fun EditText.setDoneListener(button: Button) {
    setOnEditorActionListener { v, actionId, event ->
        if ((event != null && (event.keyCode == KeyEvent.KEYCODE_ENTER)) ||
                (actionId == EditorInfo.IME_ACTION_DONE)) {
            button.performClick()
        }
        return@setOnEditorActionListener false
    }
}