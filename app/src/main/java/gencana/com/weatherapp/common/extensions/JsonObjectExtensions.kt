package gencana.com.weatherapp.common.extensions

import org.json.JSONObject

/**
 * Created by Gen Cana on 05/08/2018
 */

fun JSONObject.getStringValue(field: String): String?{
    return if (has(field)){
        var value = getString(field)
        if (value == "null"){
            null
        }else{
            value
        }
    }else{
        null
    }
}

fun JSONObject.getDoubleValue(field: String): Double?{
    return if (has(field)){
        getDouble(field)
    }else{
        null
    }
}

fun JSONObject.getIntValue(field: String): Int?{
    return if (has(field)){
        getInt(field)
    }else{
        null
    }
}

fun JSONObject.getLongValue(field: String): Long?{
    return if (has(field)){
        getLong(field)
    }else{
        null
    }
}


fun JSONObject.getBooleanValue(field: String): Boolean?{
    return if (has(field)){
        getBoolean(field)
    }else{
        null
    }
}

fun JSONObject.getJsonObjectValue(field: String): JSONObject?{
    return if (has(field)){
        getJSONObject(field)
    }else{
        null
    }
}




