package gencana.com.weatherapp.common.extensions

import gencana.com.weatherapp.common.constant.Constant

/**
 * Created by Gen Cana on 19/08/2018
 */

fun Number.toPercent(): String{
    return  "$this${Constant.PERCENT_SYMBOL}"
}

fun Number.toDegree(): String{
    return  "$this${Constant.DEGREE_SYMBOL}"
}