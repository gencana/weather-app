package gencana.com.weatherapp.common.extensions

import gencana.com.weatherapp.domain.model.BaseResponse

/**
 * Created by Gen Cana on 04/08/2018
 */

fun <T: BaseResponse<T>>String.parseFromJsonResponse(a: T): T?{

    if (this.isEmpty()) return null
    return a.parseFromJson(this)
}
