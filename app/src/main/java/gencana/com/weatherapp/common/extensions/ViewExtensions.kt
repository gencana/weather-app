package gencana.com.weatherapp.common.extensions

import android.view.View

/**
 * Created by Gen Cana on 05/08/2018
 */

fun View.visible(show: Boolean = true){
    visibility = if (show) View.VISIBLE else View.GONE
}

fun View.invisible(isInvisible: Boolean = true){
    visibility = if (isInvisible) View.INVISIBLE else View.VISIBLE
}