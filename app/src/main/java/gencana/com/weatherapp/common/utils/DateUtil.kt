package gencana.com.weatherapp.common.utils

import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by Gen Cana on 12/08/2018
 */


const val FORMAT_TIME = "hh a"
const val FORMAT_DAY = "EEEE"
const val FORMAT_DEFAULT_DATE = "yyyy-MM-dd'T'hh:mm:ss"
const val FORMAT_FULL_DATE = "yyyy-MM-dd HH:mm"
const val FORMATTED_FULL_DATE = "EEEE"
const val FORMATTED_TIME = "hh:mm a"
const val FORMAT_DATE = "yyyy-MM-dd"
const val FORMAT_CURRENT_CYCLE_HOUR = "yyyy-MM-dd:HH"
const val FORMAT_DATE_DAILY = "EEEE, MMMM d"

object DateUtil{

    private const val SERVER_TIME_ZONE = "UTC"
    private var serverTimeZone: TimeZone? = null
    private var localTimeZone: TimeZone? = null

    fun setupTimeZone(localTimeZone: String?, serverTimeZone: String? = SERVER_TIME_ZONE){
        this.serverTimeZone = TimeZone.getTimeZone(serverTimeZone)
        this.localTimeZone = TimeZone.getTimeZone(localTimeZone)
    }

    fun convertStringToDate(inputString: String, format: String = FORMAT_DEFAULT_DATE,
                            enableTimeZoneConversion: Boolean = true): Date?{
        var simpleDateFormat = SimpleDateFormat(format)
        if (enableTimeZoneConversion) {
            simpleDateFormat.timeZone = serverTimeZone
        }
        return simpleDateFormat.parse(inputString)
    }

    fun formatDate(date: Date, targetFormat: String, enableTimeZoneConversion: Boolean = true): String{
        var simpleDateFormat = SimpleDateFormat(targetFormat)
        if (enableTimeZoneConversion) {
            simpleDateFormat.timeZone = localTimeZone
        }
        return simpleDateFormat.format(date)
    }

    fun convertDateToTime(inputString: String, enableTimeZoneConversion: Boolean = true): String? {
        return convertStringToDate(inputString,
                enableTimeZoneConversion = enableTimeZoneConversion)?.let { formatDate(it, FORMAT_TIME, false) }
    }

    fun convertDateToDay(timeStamp: Long?, targetFormat: String = FORMAT_DAY): String{
        return formatDate(Date((timeStamp ?: 0) * 1000), targetFormat)
    }

    fun formatCurrentDate(inputString: String, format: String = FORMATTED_FULL_DATE): String? {
        return convertStringToDate(inputString, FORMAT_FULL_DATE)?.let {
            formatDate(it, format)
        }
    }

    fun concatenatedDateTimeConversionTimeZone(currentCyleHour: String?, hour: String?,
                                               targetFormat: String = FORMATTED_TIME): String?{
        var date = convertStringToDate(String.format("%s %s",
                currentCyleHour?.substringBefore(":"), hour), FORMAT_FULL_DATE)

        return date?.let { formatDate(it, targetFormat) }
    }

    fun dateTimeConversionTimeZone(dateTime: String, targetFormat: String = FORMATTED_TIME): String?{
        var date = convertStringToDate(dateTime, FORMAT_FULL_DATE)

        return date?.let { formatDate(it, targetFormat) }
    }

}