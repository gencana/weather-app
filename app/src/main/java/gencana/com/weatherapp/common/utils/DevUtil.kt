package gencana.com.weatherapp.common.utils

import android.util.Log

/**
 * Created by Gen Cana on 05/08/2018
 */

private const val TAG = "gencana"

fun logError(message: String){
    Log.e(TAG, message)
}