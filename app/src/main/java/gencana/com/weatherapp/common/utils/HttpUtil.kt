package gencana.com.weatherapp.common.utils

import android.net.Uri
import gencana.com.weatherapp.common.constant.ApiConstants
import gencana.com.weatherapp.common.constant.ApiConstants.ERROR_NETWORK
import gencana.com.weatherapp.common.constant.ApiConstants.ERROR_UNKNOWN
import gencana.com.weatherapp.common.constant.JsonFields
import org.json.JSONObject

/**
 * Created by Gen Cana on 05/08/2018
 */

//create uri with api parameters for http call
fun buildGetUri(endpoint: String, parameters: LinkedHashMap<String, String>, baseUrl: String): String{
    val builder = Uri.parse("$baseUrl$endpoint").buildUpon()

    for (param in parameters){
        builder.appendQueryParameter(param.key, param.value)
    }

    return builder.build().toString()
}

//validate http response
fun isResponseError(jsonResponse: String?): String?{

    if (jsonResponse.isNullOrEmpty() || jsonResponse.equals(ApiConstants.ERROR_CODE_UNKNOWN)){
        return ERROR_UNKNOWN
    }

    if (jsonResponse.equals(ApiConstants.ERROR_CODE_NETWORK)){
        return ERROR_NETWORK
    }

    if (jsonResponse == ApiConstants.ERROR_CODE_NO_CONTENT.toString()){
        return jsonResponse
    }

    var json = JSONObject(jsonResponse)
    if (json.has(JsonFields.ERROR)){
        return json.getString(JsonFields.ERROR)
    }

    if (json.has(JsonFields.DATA)){
        return null
    }

    return ERROR_UNKNOWN
}