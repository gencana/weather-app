package gencana.com.weatherapp.common.utils

/**
 * Created by Gen Cana on 07/08/2018
 */

interface HasX<out V> {
    val x: V
}

class InstanceCreator<T, P>(val x: P) {
    companion object {
        fun <T, R : HasX<P>, P> create(thingy: R) = InstanceCreator<T, P>(thingy.x)
    }
}