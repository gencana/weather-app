package gencana.com.weatherapp.common.utils

import android.content.Context
import java.io.IOException
import java.nio.charset.Charset

/**
 * Created by Gen Cana on 20/08/2018
 */
object JsonFIleUtil {

    //convert json asset file to string
    fun loadJSONFromAsset(context: Context, fileName: String): String? {
        var json: String? = null
        try {
            val inputStream = context.assets.open(fileName)
            val size = inputStream.available()
            val buffer = ByteArray(size)
            inputStream.read(buffer)
            inputStream.close()
            json = String(buffer, Charset.defaultCharset())
        } catch (ex: IOException) {
            ex.printStackTrace()
            return null
        }

        return json
    }
}