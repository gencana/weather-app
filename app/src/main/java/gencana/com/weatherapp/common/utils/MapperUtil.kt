package gencana.com.weatherapp.common.utils

import gencana.com.weatherapp.common.constant.Constant
import gencana.com.weatherapp.common.constant.JsonFields
import gencana.com.weatherapp.common.extensions.*
import gencana.com.weatherapp.common.utils.DateUtil.concatenatedDateTimeConversionTimeZone
import gencana.com.weatherapp.common.utils.DateUtil.convertDateToDay
import gencana.com.weatherapp.data.remote.http.ApiResponse
import gencana.com.weatherapp.domain.model.*
import gencana.com.weatherapp.view.main.WeatherListDataModel
import org.json.JSONObject
import java.util.*
import kotlin.collections.ArrayList
import kotlin.reflect.KClass

/**
 * Created by Gen Cana on 05/08/2018
 */

fun convertPairToMap(keys: Array<String>, values: Array<String>): LinkedHashMap<String, String>{
    if (keys.size != values.size){
        throw IndexOutOfBoundsException("Keys and Values must be of equal size!!!")
    }

    val map = LinkedHashMap<String, String>()
    if (keys.isEmpty() || values.isEmpty()){
        return map
    }

    for (index in keys.indices){
        map.put(keys[index], values[index])
    }

    return map
}

fun <T: BaseResponse<T>>convertJsonToApiResponse(jsonString: String?, model: KClass<T>): ApiResponse<T>{
    if (jsonString.isNullOrEmpty()){
        return ApiResponse()
    }

    var apiResponse = ApiResponse<T>()
    val list = ArrayList<T>()
    val mainJson = JSONObject(jsonString)
    val dataArray = mainJson.getJSONArray(JsonFields.DATA) ?: return apiResponse

    for (i in 0..(dataArray.length() - 1)) {
        val itemString = dataArray.getJSONObject(i).toString()
        var data = itemString.parseFromJsonResponse(model.java.newInstance())
        if (data != null){
            data.baseStateCode = mainJson.getStringValue(JsonFields.STATE_CODE)
            data.baseCityName = mainJson.getStringValue(JsonFields.CITY_NAME)
            data.baseLongitude = mainJson.getDoubleValue(JsonFields.LONGITUDE)
            data.baseLatitude = mainJson.getDoubleValue(JsonFields.LATITUDE)
            data.baseTimeZone = mainJson.getStringValue(JsonFields.TIMEZONE)
            data.baseCountryCode = mainJson.getStringValue(JsonFields.COUNTRY_CODE)

            list.add(data)
        }
    }
    apiResponse.data = list
    return apiResponse
}

fun convertToExtendedWeatherList(viewType: Int, data: WeatherModel?): ArrayList<WeatherListDataModel>{
    var extendedWeatherList = ArrayList<WeatherListDataModel>()
    if (data != null) {
        extendedWeatherList.add(WeatherListDataModel(viewType, WeatherExtendedModel(
                Constant.LABEL_SUNRISE, data.timezone?.let { concatenatedDateTimeConversionTimeZone(data.dateTime, data.sunrise) },
                Constant.LABEL_SUNSET, data.timezone?.let { concatenatedDateTimeConversionTimeZone(data.dateTime, data.sunset) })))
        extendedWeatherList.add(WeatherListDataModel(viewType, WeatherExtendedModel(
                Constant.LABEL_CHANCE_OF_RAIN, data.dewPoint?.toPercent(),
                Constant.LABEL_HUMIDITY, data.humidity?.toPercent())))
        extendedWeatherList.add(WeatherListDataModel(viewType, WeatherExtendedModel(
                Constant.LABEL_WIND, String.format("%s %.2f %s", data.windCdir, data.windSpeed, Constant.WIND_SPEED_SYMBOL),
                Constant.LABEL_APPARENT_TEMP, data.apparentTemperature?.toDegree())))
        extendedWeatherList.add(WeatherListDataModel(viewType, WeatherExtendedModel(
                Constant.LABEL_COUNTRY, data.countryCode,
                Constant.LABEL_STATE, data.stateCode, false)))
    }
    return extendedWeatherList
}

fun convertToDailyWeatherList(viewType: Int, data: WeatherDaily): ArrayList<WeatherListDataModel>{
    var extendedWeatherList = ArrayList<WeatherListDataModel>()
    extendedWeatherList.add(WeatherListDataModel(viewType, WeatherExtendedModel(
            Constant.LABEL_SUNRISE,
            data.sunrise?.let { convertDateToDay(it, FORMATTED_TIME) },
            Constant.LABEL_SUNSET,
            data.sunset?.let { convertDateToDay(it, FORMATTED_TIME) })))
    extendedWeatherList.add(WeatherListDataModel(viewType, WeatherExtendedModel(
            Constant.LABEL_CHANCE_OF_RAIN, data.dewPoint?.toPercent(),
            Constant.LABEL_HUMIDITY, data.humidity?.toPercent())))
    extendedWeatherList.add(WeatherListDataModel(viewType, WeatherExtendedModel(
            Constant.LABEL_WIND, String.format("%s %.2f %s", data.windCdir, data.windSpeed, Constant.WIND_SPEED_SYMBOL),
            Constant.LABEL_SNOW, "${data.snow?.toString()} ${Constant.MILLIMETER}")))
    extendedWeatherList.add(WeatherListDataModel(viewType, WeatherExtendedModel(
            Constant.LABEL_PRESSURE, "${data.pressure} ${Constant.PRESSURE_SYMBOL}",
            Constant.LABEL_UV, data.uv.toString())))
    extendedWeatherList.add(WeatherListDataModel(viewType, WeatherExtendedModel(
            Constant.LABEL_CLOUDS_LOW, data.cloudsLow?.toPercent(),
            Constant.LABEL_CLOUDS_HI, data.cloudsHi?.toPercent())))
    extendedWeatherList.add(WeatherListDataModel(viewType, WeatherExtendedModel(
            Constant.LABEL_MOONRISE, data.moonRise?.let { convertDateToDay(it, FORMATTED_TIME) },
            Constant.LABEL_MOONSET, data.moonSet?.let { convertDateToDay(it, FORMATTED_TIME) })))
    extendedWeatherList.add(WeatherListDataModel(viewType, WeatherExtendedModel(
            Constant.LABEL_COUNTRY, data.baseCountryCode,
            Constant.LABEL_STATE, data.baseStateCode, false)))

    return extendedWeatherList
}