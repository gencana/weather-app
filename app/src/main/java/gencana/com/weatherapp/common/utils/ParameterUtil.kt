package gencana.com.weatherapp.common.utils

import android.location.Location
import gencana.com.weatherapp.BuildConfig
import gencana.com.weatherapp.common.constant.ApiConstants
import gencana.com.weatherapp.common.constant.ApiConstants.MAX_DAYS

/**
 * Created by Gen Cana on 05/08/2018
 */

fun getCurrentWeatherParams(location: Location?, city: String?): LinkedHashMap<String, String>?{
    if (location == null && city == null){
        return null
    }

    lateinit var keys: Array<String>
    lateinit var values: Array<String>
    if (location != null) {
        keys = arrayOf(ApiConstants.PARAM_API_KEY, ApiConstants.PARAM_LATITUDE, ApiConstants.PARAM_LONGITUDE)
        values = arrayOf(BuildConfig.WEATHERBIT_API_KEY, location.latitude.toString(), location.longitude.toString())
    }else{
        if (city != null) {
            keys = arrayOf(ApiConstants.PARAM_API_KEY, ApiConstants.PARAM_CITY)
            values = arrayOf(BuildConfig.WEATHERBIT_API_KEY, city)
        }
    }
    return convertPairToMap(keys, values)
}

fun getDailyWeatherParams(location: Location?, city: String?, days: Int = MAX_DAYS): LinkedHashMap<String, String>?{
    val map = getCurrentWeatherParams(location, city)
    map?.put(ApiConstants.PARAM_DAYS, days.toString())
    return map
}