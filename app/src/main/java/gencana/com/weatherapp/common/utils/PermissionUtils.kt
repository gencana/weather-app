package gencana.com.weatherapp.common.utils

import android.content.DialogInterface
import android.widget.Toast
import android.support.v4.app.ActivityCompat
import android.content.pm.PackageManager
import android.support.v4.content.ContextCompat
import android.os.Build
import android.app.Activity
import android.app.AlertDialog


/**
 * Created by Gen Cana on 19/08/2018
 */
class PermissionUtils {

    internal var context: Activity

    private var permissionResultCallback: PermissionResultCallback


    private var permission_list: ArrayList<String> = ArrayList()
    private var listPermissionsNeeded: ArrayList<String> = ArrayList()

    private var dialog_content = ""
    private var req_code: Int = 0

    constructor(context: Activity, callback: PermissionResultCallback) {
        this.context = context

        permissionResultCallback = callback


    }

    fun check_permission(permissions: ArrayList<String>, dialog_content: String, request_code: Int) {
        this.permission_list = permissions
        this.dialog_content = dialog_content
        this.req_code = request_code

        if (Build.VERSION.SDK_INT >= 23) {
            if (checkAndRequestPermissions(permissions, request_code)) {
                permissionResultCallback.onPermissionGranted(request_code)
            }
        } else {
            permissionResultCallback.onPermissionGranted(request_code)
        }

    }

    private fun checkAndRequestPermissions(permissions: ArrayList<String>, request_code: Int): Boolean {

        if (permissions.size > 0) {
            listPermissionsNeeded = ArrayList()

            for (i in 0 until permissions.size) {
                val hasPermission = ContextCompat.checkSelfPermission(context, permissions[i])

                if (hasPermission != PackageManager.PERMISSION_GRANTED) {
                    listPermissionsNeeded.add(permissions[i])
                }

            }

            if (!listPermissionsNeeded.isEmpty()) {
                ActivityCompat.requestPermissions(context, listPermissionsNeeded.toArray(arrayOfNulls(listPermissionsNeeded.size)), request_code)
                return false
            }
        }

        return true
    }

    fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            1 -> if (grantResults.isNotEmpty()) {
                val perms = HashMap<String, Int>()

                for (i in permissions.indices) {
                    perms.put(permissions[i], grantResults[i])
                }

                val pendingPermissions = ArrayList<String>()

                for (i in 0 until listPermissionsNeeded.size) {
                    if (perms.get(listPermissionsNeeded[i]) != PackageManager.PERMISSION_GRANTED) {
                        if (ActivityCompat.shouldShowRequestPermissionRationale(context, listPermissionsNeeded[i]))
                            pendingPermissions.add(listPermissionsNeeded[i])
                        else {
                            permissionResultCallback.onNeverAskAgain(req_code)
                            Toast.makeText(context, "Go to settings and enable permissions", Toast.LENGTH_LONG).show()
                            return
                        }
                    }

                }

                if (pendingPermissions.size > 0) {
                    showMessageOKCancel(dialog_content,
                            DialogInterface.OnClickListener { dialog, which ->
                                when (which) {
                                    DialogInterface.BUTTON_POSITIVE -> check_permission(permission_list, dialog_content, req_code)
                                    DialogInterface.BUTTON_NEGATIVE -> {
                                        if (permission_list.size == pendingPermissions.size)
                                            permissionResultCallback.onPermissionDenied(req_code)
                                        else
                                            permissionResultCallback.onPartialPermissionGranted(req_code, pendingPermissions)
                                    }
                                }
                            })

                } else {
                    permissionResultCallback.onPermissionGranted(req_code)

                }


            }
        }
    }

    private fun showMessageOKCancel(message: String, okListener: DialogInterface.OnClickListener) {
        AlertDialog.Builder(context)
                .setMessage(message)
                .setPositiveButton("Ok", okListener)
                .setNegativeButton("Cancel", okListener)
                .create()
                .show()
    }

    interface PermissionResultCallback {
        fun onPermissionGranted(request_code: Int)
        fun onPartialPermissionGranted(request_code: Int, granted_permissions: ArrayList<String>)
        fun onPermissionDenied(request_code: Int)
        fun onNeverAskAgain(request_code: Int)
    }
}

