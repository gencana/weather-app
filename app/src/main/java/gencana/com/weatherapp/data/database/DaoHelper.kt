package gencana.com.weatherapp.data.database

import android.app.Application
import android.database.sqlite.SQLiteDatabase
import gencana.com.weatherapp.common.application.WeatherApplication
import gencana.com.weatherapp.common.constant.Constant
import gencana.com.weatherapp.common.constant.DatabaseConstant
import gencana.com.weatherapp.common.extensions.parseFromJsonResponse
import gencana.com.weatherapp.common.utils.JsonFIleUtil
import gencana.com.weatherapp.data.database.dao.CityListDaoImpl
import gencana.com.weatherapp.data.database.dao.Dao
import gencana.com.weatherapp.domain.model.City
import org.json.JSONArray

/**
 * Created by Gen Cana on 01/09/2018
 */
object DaoHelper : DatabaseHelper.DatabaseListener {

    private var dbHelper: DatabaseHelper? = null

    private var cityDao: Dao<String>? = null

    private lateinit var app: Application

    fun init(app: WeatherApplication){
        this.app = app
        dbHelper = DatabaseHelper(app, this)
        cityDao = CityListDaoImpl(dbHelper!!)
    }

    override fun onDatabaseCreate(db: SQLiteDatabase) {
        createCityTable(db)
        insertCitiesFromJson()
    }

    fun addCities(cityList: HashSet<String>){
        throwIfNull()
        cityDao!!.insert(cityList)
    }

    fun searchCity(city: String): List<String>?{
        throwIfNull()
        return cityDao!!.search(city)
    }

    private fun throwIfNull(){
        if (cityDao == null){
            throw NullPointerException("Failed to initialize DaoHelper")
        }
    }

    private fun createCityTable(db: SQLiteDatabase){
        db.execSQL("CREATE TABLE IF NOT EXISTS ${DatabaseConstant.TBL_CITY} " +
                "(${DatabaseConstant.COLUMN_CITY} TEXT UNIQUE)")
    }

    /**
     * This method inserts around 35k cities with over 15k populations
     * from json assets file to local db. This is invoked onDatabaseCreate
     */
    private fun insertCitiesFromJson(){
        Thread(Runnable {
            var cityListJson = JsonFIleUtil.loadJSONFromAsset(app, Constant.JSON_FILE_CITY)
            var cityList = HashSet<String>()
            var cityJsonArray = JSONArray(cityListJson)

            for (i in 0..(cityJsonArray.length() - 1)) {
                val cityObject = cityJsonArray.getJSONObject(i).toString()
                var city = cityObject.parseFromJsonResponse(City())
                city?.cityName?.let { cityList.add(it) }
            }
            addCities(cityList)
        }).start()
    }
}