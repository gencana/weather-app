package gencana.com.weatherapp.data.database

import android.app.Application
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import gencana.com.weatherapp.common.constant.DatabaseConstant
import gencana.com.weatherapp.common.utils.SingletonHolder

/**
 * Created by Gen Cana on 01/09/2018
 */
class DatabaseHelper(context: Application, private var databaseListener: DatabaseListener) :
        SQLiteOpenHelper(context, DatabaseConstant.DB_NAME, null, DatabaseConstant.DB_VERSION) {

    interface DatabaseListener {
        fun onDatabaseCreate(db: SQLiteDatabase)
    }

    companion object: SingletonHolder<DatabaseHelper, Application, DatabaseListener>(::DatabaseHelper)

    override fun onCreate(db: SQLiteDatabase) {
        databaseListener.onDatabaseCreate(db)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
    }

}