package gencana.com.weatherapp.data.database.dao

import android.database.sqlite.SQLiteDatabase
import gencana.com.weatherapp.data.database.DatabaseHelper
import gencana.com.weatherapp.common.constant.DatabaseConstant


/**
 * Created by Gen Cana on 01/09/2018
 */

class CityListDaoImpl(databaseHelper: DatabaseHelper) : Dao<String> {

    private var dbReadable: SQLiteDatabase = databaseHelper.readableDatabase
    private var dbWriteable: SQLiteDatabase = databaseHelper.writableDatabase

    private val searchLimit = 10

    //search city
    override fun search(key: String): List<String>? {
        val cityList = ArrayList<String>()
        val cursor = dbReadable.rawQuery(
                "SELECT * FROM ${DatabaseConstant.TBL_CITY} WHERE ${DatabaseConstant.COLUMN_CITY} LIKE ? LIMIT $searchLimit",
                arrayOf("$key%"))
        if (cursor.count > 0){
            while (cursor.moveToNext()){
                cityList.add(cursor.getString(cursor.getColumnIndex(DatabaseConstant.COLUMN_CITY)))
            }
        }

        cursor.close()

        return cityList
    }

    //batch insert of cities. This should be called from background thread
    override fun insert(any: HashSet<String>) {
        val sql = "INSERT INTO ${DatabaseConstant.TBL_CITY} VALUES(?)"
        val statement = dbWriteable.compileStatement(sql)
        dbWriteable.beginTransaction()
        try {
            for (city in any) {
                statement.clearBindings()
                statement.bindString(1, city)
                statement.execute()
            }
            dbWriteable.setTransactionSuccessful()
        } finally {
            dbWriteable.endTransaction()
        }

    }

}