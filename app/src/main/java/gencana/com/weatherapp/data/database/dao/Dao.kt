package gencana.com.weatherapp.data.database.dao

/**
 * Created by Gen Cana on 01/09/2018
 */

interface Dao<T> {

    fun search(key: String): List<T>?

    fun insert(any: HashSet<T>)
}
