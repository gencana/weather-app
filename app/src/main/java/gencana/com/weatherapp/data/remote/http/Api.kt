package gencana.com.weatherapp.data.remote.http

import android.os.AsyncTask
import gencana.com.weatherapp.common.constant.ApiConstants
import gencana.com.weatherapp.domain.model.WeatherDaily
import gencana.com.weatherapp.domain.model.WeatherHourly
import gencana.com.weatherapp.domain.model.WeatherModel

/**
 * Created by Gen Cana on 05/08/2018
 */
object Api {

    fun getCurrentWeather(hashMap: LinkedHashMap<String, String>,
                             callback: HttpGetCallTask.ResponseCallback<WeatherModel>) {
        HttpGetCallTask(
                WeatherModel::class,
                ApiConstants.ENDPOINT_CURRENT_WEATHER,
                hashMap,
                callback
        ).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR)

    }

    fun getHourlyWeather(hashMap: LinkedHashMap<String, String>,
                             callback: HttpGetCallTask.ResponseCallback<WeatherHourly>) {
        HttpGetCallTask(
                WeatherHourly::class,
                ApiConstants.ENDPOINT_FORECAST_HOURLY,
                hashMap,
                callback
        ).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR)

    }

    fun getDailyWeather(hashMap: LinkedHashMap<String, String>,
                         callback: HttpGetCallTask.ResponseCallback<WeatherDaily>) {
        HttpGetCallTask(
                WeatherDaily::class,
                ApiConstants.ENDPOINT_FORECAST_DAILY,
                hashMap,
                callback
        ).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR)

    }
}