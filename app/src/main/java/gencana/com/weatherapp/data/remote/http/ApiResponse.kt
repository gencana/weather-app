package gencana.com.weatherapp.data.remote.http

/**
 * Created by Gen Cana on 05/08/2018
 */
class ApiResponse<T> {

    var error: String? = null

    var data: List<T>? = null

    var count: Int? = null

    var cityName: String? = null

    var longitude: Double? = null

    var latitude: Double? = null

    var timeZone: String? = null

    var countryCode: String? = null

    var stateCode: String? = null
}