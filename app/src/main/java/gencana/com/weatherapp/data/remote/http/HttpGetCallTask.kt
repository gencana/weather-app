package gencana.com.weatherapp.data.remote.http

import android.os.AsyncTask
import gencana.com.weatherapp.common.utils.convertJsonToApiResponse
import gencana.com.weatherapp.common.utils.isResponseError
import gencana.com.weatherapp.domain.model.BaseResponse
import kotlin.reflect.KClass

/**
 * Created by Gen Cana on 04/08/2018
 */

/**
 * This is a generic asynctask that process http call
 */
class HttpGetCallTask<T: BaseResponse<T>> (
        private var t: KClass<T>,
        private var endpoint: String,
        private var parameters: LinkedHashMap<String, String>,
        private val responseCallback: ResponseCallback<T>
) : AsyncTask<String, Void, ApiResponse<T>>() {

    override fun doInBackground(vararg strings: String?): ApiResponse<T> {
        val jsonResponse = HttpManager.doGet(endpoint, parameters)

        var apiResponse = ApiResponse<T>()
        var errorMessage = isResponseError(jsonResponse)
        if (errorMessage != null){
            apiResponse.error = errorMessage
        }else{
            apiResponse = convertJsonToApiResponse(jsonResponse, t)
        }

        return apiResponse
    }

    override fun onPostExecute(apiResponse: ApiResponse<T>) {
        super.onPostExecute(apiResponse)
        if (apiResponse.error != null){
            responseCallback.onResultFailed(apiResponse.error!!)
        }else{
            if (apiResponse.data != null){
                responseCallback.onResultSuccess(apiResponse)

            }

        }
    }

    interface ResponseCallback<T> {

        fun onResultFailed(message: String)

        fun onResultSuccess(response: ApiResponse<T>)
    }

}
