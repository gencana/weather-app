package gencana.com.weatherapp.data.remote.http

import gencana.com.weatherapp.BuildConfig
import gencana.com.weatherapp.common.constant.ApiConstants
import java.io.*
import java.net.HttpURLConnection
import java.net.URL
import gencana.com.weatherapp.common.utils.buildGetUri
import gencana.com.weatherapp.common.utils.logError
import kotlin.collections.LinkedHashMap


/**
 * Created by Gen Cana on 04/08/2018
 */
internal object HttpManager {

    private const val RESPONSE_CODE_SUCCESS = 200

    fun doGet(endpoint: String, parameters: LinkedHashMap<String, String>,
              baseUrl :String = BuildConfig.WEATHERBIT_BASE_URL): String? {
        var stream: String?
        var br: BufferedReader? = null
        var isr: InputStreamReader? = null
        var inputStream: InputStream? = null
        try {
            var stringUrl: String = buildGetUri(endpoint, parameters, baseUrl)
            logError("url is $stringUrl")
            val url = URL(stringUrl)
            val urlConnection = url.openConnection() as HttpURLConnection

            // Check the connection status
            if (urlConnection.responseCode == RESPONSE_CODE_SUCCESS) {
                // if response code = 200 ok
                inputStream = BufferedInputStream(urlConnection.inputStream)

                // Read the BufferedInputStream
                isr = InputStreamReader(inputStream)
                br = BufferedReader(isr)
                val sb = StringBuilder()

                var line: String? = null
                while ({ line = br.readLine(); line }() != null) {
                    sb.append(line)
                }

                stream = sb.toString()
                // End reading...............

                // Disconnect the HttpURLConnection
                urlConnection.disconnect()
            } else {
                // Do something
                if (urlConnection.responseCode == ApiConstants.ERROR_CODE_NO_CONTENT) {
                    throw NoSuchElementException()
                }
                throw RuntimeException("HTTP ERROR: " + urlConnection.responseCode)
            }
        }catch (e: NoSuchElementException){
            stream = ApiConstants.ERROR_CODE_NO_CONTENT.toString()
        } catch (e: IOException) {
            stream = ApiConstants.ERROR_CODE_NETWORK
            e.printStackTrace()
        } catch (e: Throwable) {
            stream = ApiConstants.ERROR_CODE_UNKNOWN
            e.printStackTrace()
        } finally {
            try {
                br?.close()

                isr?.close()

                inputStream?.close()
            } catch (e: IOException) {
                stream = ApiConstants.ERROR_CODE_UNKNOWN
                e.printStackTrace()
            }

        }
        // Return the data from specified url
        return stream
    }


}
