package gencana.com.weatherapp.domain.model

import java.io.Serializable


/**
 * Created by Gen Cana on 04/08/2018
 */
abstract class BaseResponse<T>: Serializable {

    var baseCityName: String? = null

    var baseLongitude: Double? = null

    var baseLatitude: Double? = null

    var baseTimeZone: String? = null

    var baseCountryCode: String? = null

    var baseStateCode: String? = null

    //abstract method used to manually parse from json string to pojo
    abstract fun parseFromJson(jsonString: String): T?

}