package gencana.com.weatherapp.domain.model

import gencana.com.weatherapp.common.constant.JsonFields
import gencana.com.weatherapp.common.extensions.getStringValue
import org.json.JSONObject

/**
 * Created by Gen Cana on 20/08/2018
 */
class City: BaseResponse<City>() {

    var cityName: String? = null

    override fun parseFromJson(jsonString: String): City? {
        val json = JSONObject(jsonString)
        val city = City()

        city.cityName = json.getStringValue(JsonFields.CITY_NAME)
        var state = json.getStringValue(JsonFields.STATE_NAME)
        if (state != null){
            city.cityName += ", $state"
        }

        return city
    }
}