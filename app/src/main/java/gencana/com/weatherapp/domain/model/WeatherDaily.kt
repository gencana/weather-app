package gencana.com.weatherapp.domain.model

import gencana.com.weatherapp.common.constant.JsonFields
import gencana.com.weatherapp.common.extensions.*
import org.json.JSONObject

/**
 * Created by Gen Cana on 07/08/2018
 */

class WeatherDaily: BaseResponse<WeatherDaily>(){

    var timeStamp: Long? = null
    var weatherIcon: String? = null
    var weatherDescription: String? = null
    var minimumTemperature: Double? = null
    var maximumTemperature: Double? = null
    var temperature: Double? = null

    var dewPoint: Double? = null
    var pressure: Int? = null
    var windCdir: String? = null
    var windCdirFull: String? = null
    var humidity: Int? = null
    var windSpeed: Double? = null
    var dateTime: String? = null
    var sunset: Long? = null
    var sunrise: Long? = null
    var uv: Double? = null
    var cloudsLow: Int? = null
    var cloudsHi: Int? = null
    var moonSet: Long? = null
    var moonRise: Long? = null
    var snow: Int? = null


    override fun parseFromJson(jsonString: String): WeatherDaily? {
        val json = JSONObject(jsonString)
        val weather = WeatherDaily()

        weather.timeStamp = json.getLongValue(JsonFields.SUNRISE_TIMESTAMP)
        weather.minimumTemperature = json.getDoubleValue(JsonFields.MIN_TEMPERATURE)
        weather.maximumTemperature = json.getDouble(JsonFields.MAX_TEMPERATURE)
        weather.temperature = json.getDouble(JsonFields.TEMP)

        var jsonWeather = json.getJsonObjectValue(JsonFields.WEATHER)
        if (jsonWeather != null) {
            weather.weatherIcon = jsonWeather.getStringValue(JsonFields.WEATHER_ICON)
            weather.weatherDescription = jsonWeather.getStringValue(JsonFields.WEATHER_DESCRIPTION)
        }

        weather.dewPoint = json.getDoubleValue(JsonFields.DEW_POINT)
        weather.pressure = json.getIntValue(JsonFields.PRESSURE)
        weather.windCdir = json.getStringValue(JsonFields.WIND_CDIR)
        weather.windCdirFull = json.getStringValue(JsonFields.WIND_DIRECTION_DISPLAY)
        weather.humidity = json.getIntValue(JsonFields.HUMIDITY)
        weather.windSpeed = json.getDoubleValue(JsonFields.WIND_SPEED)
        weather.dateTime = json.getStringValue(JsonFields.DATETIME)
        weather.sunset = json.getLongValue(JsonFields.SUNSET_TIMESTAMP)
        weather.sunrise = json.getLongValue(JsonFields.SUNRISE_TIMESTAMP)
        weather.uv = json.getDoubleValue(JsonFields.UV)
        weather.cloudsLow = json.getIntValue(JsonFields.CLOUDS_LOW)
        weather.cloudsHi = json.getIntValue(JsonFields.CLOUDS_HI)
        weather.moonSet = json.getLongValue(JsonFields.MOONSET)
        weather.moonRise = json.getLongValue(JsonFields.MOONRISE)
        weather.snow = json.getIntValue(JsonFields.SNOW)

        return weather
    }
}