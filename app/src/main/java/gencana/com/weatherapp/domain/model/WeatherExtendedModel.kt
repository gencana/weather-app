package gencana.com.weatherapp.domain.model

/**
 * Created by Gen Cana on 12/08/2018
 */

data class WeatherExtendedModel(
        var textLabelLeft: String? = null,
        var textDetailLeft: String? = null,
        var textLabelRight: String? = null,
        var textDetailRight: String? = null,
        var showDivider: Boolean = true
)