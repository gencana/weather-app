package gencana.com.weatherapp.domain.model

import gencana.com.weatherapp.common.constant.JsonFields
import gencana.com.weatherapp.common.extensions.getDoubleValue
import gencana.com.weatherapp.common.extensions.getIntValue
import gencana.com.weatherapp.common.extensions.getJsonObjectValue
import gencana.com.weatherapp.common.extensions.getStringValue
import org.json.JSONObject

/**
 * Created by Gen Cana on 06/08/2018
 */
class WeatherHourly: BaseResponse<WeatherHourly>() {

    var weatherIcon: String? = null
    var clouds: Int? = null
    var temperature: Double? = null
    var time: String? = null

    override fun parseFromJson(jsonString: String): WeatherHourly? {

        val json = JSONObject(jsonString)
        val weather = WeatherHourly()

        weather.clouds = json.getIntValue(JsonFields.CLOUDS)
        weather.temperature = json.getDoubleValue(JsonFields.TEMP)
        weather.time = json.getStringValue(JsonFields.TIMESTAMP_LOCAL)

        var jsonWeather = json.getJsonObjectValue(JsonFields.WEATHER)
        if (jsonWeather != null) {
            weather.weatherIcon = jsonWeather.getStringValue(JsonFields.WEATHER_ICON)
        }

        return weather
    }
}