package gencana.com.weatherapp.domain.model

import gencana.com.weatherapp.common.constant.JsonFields
import gencana.com.weatherapp.common.extensions.*
import org.json.JSONException
import org.json.JSONObject

/**
 * Created by Gen Cana on 04/08/2018
 */

class WeatherModel: BaseResponse<WeatherModel>() {

    var windCdir: String? = null
    var humidity: Int? = null
    var dayPart: String? = null
    var longitude: Double? = null
    var pressure: Int? = null
    var timezone: String? = null
    var observationTime: String? = null
    var countryCode: String? = null
    var clouds: Int? = null
    var visbility: Int? = null
    var stateCode: String? = null
    var windSpeed: Double? = null
    var latitude: Double? = null
    var windCdirFull: String? = null
    var seaLevelPressure: Int? = null
    var dateTime: String? = null
    var timestamp: Long? = null
    var station: String? = null
    var hourAngle: Double? = null
    var dewPoint: Double? = null
    var uv: Double? = null
    var dni: Double? = null
    var windDir: Int? = null
    var elevationAngle: Double? = null
    var ghi: Double? = null
    var dhi: Double? = null
    var cityName: String? = null
    var weatherIcon: String? = null
    var weatherCode: String? = null
    var weatherDescription: String? = null
    var sunset: String? = null
    var temp: Double? = null
    var sunrise: String? = null
    var apparentTemperature: Double? = null


    override fun parseFromJson(jsonString: String): WeatherModel? {
        return try {
            val json = JSONObject(jsonString)
            val weather = WeatherModel()
            weather.windCdir = json.getStringValue(JsonFields.WIND_CDIR)
            weather.dayPart = json.getStringValue(JsonFields.DAY_PART)
            weather.pressure = json.getIntValue(JsonFields.PRESSURE)
            weather.longitude = json.getDoubleValue(JsonFields.LONGITUDE)
            weather.humidity = json.getIntValue(JsonFields.HUMIDITY)
            weather.timezone = json.getStringValue(JsonFields.TIMEZONE)
            weather.observationTime = json.getStringValue(JsonFields.OBSERVATION_TIME)
            weather.countryCode = json.getStringValue(JsonFields.COUNTRY_CODE)
            weather.clouds = json.getIntValue(JsonFields.CLOUDS)
            weather.visbility = json.getIntValue(JsonFields.VISIBILITY)
            weather.windSpeed = json.getDoubleValue(JsonFields.WIND_SPEED)
            weather.stateCode = json.getStringValue(JsonFields.STATE_CODE)
            weather.latitude = json.getDoubleValue(JsonFields.LATITUDE)
            weather.windCdirFull = json.getStringValue(JsonFields.WIND_DIRECTION_DISPLAY)
            weather.seaLevelPressure = json.getIntValue(JsonFields.SEA_LEVEL_PRESSURE)
            weather.dateTime = json.getStringValue(JsonFields.DATETIME)
            weather.timestamp = json.getLongValue(JsonFields.TIMESTAMP)
            weather.station = json.getStringValue(JsonFields.STATION)
            weather.hourAngle = json.getDoubleValue(JsonFields.HOUR_ANGLE)
            weather.dewPoint = json.getDoubleValue(JsonFields.DEW_POINT)
            weather.uv = json.getDoubleValue(JsonFields.UV)
            weather.dni = json.getDoubleValue(JsonFields.DNI)
            weather.windDir = json.getIntValue(JsonFields.WIND_DIRECTION)
            weather.elevationAngle = json.getDoubleValue(JsonFields.ELEVATION_ANGLE)
            weather.ghi = json.getDoubleValue(JsonFields.GHI)
            weather.dhi = json.getDoubleValue(JsonFields.DHI)
            weather.cityName = json.getStringValue(JsonFields.CITY_NAME)
            weather.apparentTemperature = json.getDoubleValue(JsonFields.APPARENT_TEMP)
            weather.sunset = json.getStringValue(JsonFields.SUNSET)
            weather.temp = json.getDoubleValue(JsonFields.TEMP)
            weather.sunrise = json.getStringValue(JsonFields.SUNRISE)

            var jsonWeather = json.getJsonObjectValue(JsonFields.WEATHER)
            if (jsonWeather != null) {
                weather.weatherIcon = jsonWeather.getStringValue(JsonFields.WEATHER_ICON)
                weather.weatherCode = jsonWeather.getStringValue(JsonFields.WEATHER_CODE)
                weather.weatherDescription = jsonWeather.getStringValue(JsonFields.WEATHER_DESCRIPTION)
            }

            weather
        } catch (e: JSONException) {
            e.printStackTrace()
            null
        }
    }

    override fun equals(other: Any?): Boolean {
        if (other === null || other !is WeatherModel){
            return false
        }

        return (other.windCdir == windCdir &&
                other.humidity == humidity &&
                other.dayPart == dayPart &&
                other.longitude == longitude &&
                other.pressure == pressure &&
                other.timezone == timezone &&
                other.observationTime == observationTime &&
                other.countryCode == countryCode &&
                other.clouds == clouds &&
                other.visbility == visbility &&
                other.stateCode == stateCode &&
                other.windSpeed == windSpeed &&
                other.latitude == latitude &&
                other.windCdirFull == windCdirFull &&
                other.seaLevelPressure == seaLevelPressure &&
                other.dateTime == dateTime &&
                other.timestamp == timestamp &&
                other.station == station &&
                other.hourAngle == hourAngle &&
                other.dewPoint == dewPoint &&
                other.uv == uv &&
                other.dni == dni &&
                other.windDir == windDir &&
                other.elevationAngle == elevationAngle &&
                other.ghi == ghi &&
                other.cityName == cityName &&
                other.weatherIcon == weatherIcon &&
                other.weatherCode == weatherCode &&
                other.weatherDescription == weatherDescription &&
                other.sunset == sunset &&
                other.temp == temp &&
                other.sunrise == sunrise &&
                other.apparentTemperature == apparentTemperature)
    }
}