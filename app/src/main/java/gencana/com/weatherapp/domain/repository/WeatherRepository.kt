package gencana.com.weatherapp.domain.repository

import gencana.com.weatherapp.data.remote.http.Api
import gencana.com.weatherapp.data.remote.http.HttpGetCallTask
import gencana.com.weatherapp.domain.model.WeatherDaily
import gencana.com.weatherapp.domain.model.WeatherHourly
import gencana.com.weatherapp.domain.model.WeatherModel

/**
 * Created by Gen Cana on 05/08/2018
 */
object WeatherRepository {

    fun getCurrentWeather(hashMap: LinkedHashMap<String, String>,
                          callback: HttpGetCallTask.ResponseCallback<WeatherModel>){
        Api.getCurrentWeather(hashMap, callback)
    }

    fun getHourlyWeather(hashMap: LinkedHashMap<String, String>,
                          callback: HttpGetCallTask.ResponseCallback<WeatherHourly>){
        Api.getHourlyWeather(hashMap, callback)
    }

    fun getDailyWeather(hashMap: LinkedHashMap<String, String>,
                         callback: HttpGetCallTask.ResponseCallback<WeatherDaily>){
        Api.getDailyWeather(hashMap, callback)
    }
}