package gencana.com.weatherapp.view.changelocation

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import gencana.com.weatherapp.R
import gencana.com.weatherapp.common.base.BaseActivity
import gencana.com.weatherapp.common.constant.Constant
import gencana.com.weatherapp.common.extensions.hideKeyboard
import kotlinx.android.synthetic.main.activity_change_location.*
import kotlin.reflect.KClass
import android.widget.ArrayAdapter
import gencana.com.weatherapp.common.extensions.setDoneListener
import gencana.com.weatherapp.data.database.DaoHelper


/**
 * Created by Gen Cana on 20/08/2018
 */
class ChangeLocationActivity: BaseActivity<Nothing, Nothing>(), TextWatcher {

    companion object {

        fun startActivityForResult(activity: Activity, requestCode: Int = 10){
            var intent = Intent(activity, ChangeLocationActivity::class.java)
            activity.startActivityForResult(intent, requestCode)
        }
    }

    private var cityList = ArrayList<String>()

    private lateinit var adapter: ArrayAdapter<String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        showBackButton()
        adapter = ArrayAdapter(this, android.R.layout.select_dialog_item,cityList)
        et_city.setAdapter(adapter)
        et_city.addTextChangedListener(this)
        et_city.setDoneListener(btn_go)

        btn_go.setOnClickListener{
            if (!et_city.text.toString().trim().isEmpty()){
                hideKeyboard()
                var intent = Intent()
                intent.putExtra(Constant.KEY_CITY, et_city.text.toString())
                setResult(Activity.RESULT_OK, intent)
                finish()
            }
        }

        et_city.setOnItemClickListener { parent, view, position, id ->
            btn_go.performClick()
        }

    }

    override fun onResponseFailed(message: String?) {
    }

    override fun onResponseSuccess(data: Nothing) {
    }

    override fun getLayout(): Int {
        return R.layout.activity_change_location
    }

    override fun getViewModel(): KClass<Nothing>? {
        return null
    }

    override fun afterTextChanged(s: Editable?) {

    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        adapter.clear()
        adapter.addAll(DaoHelper.searchCity(et_city.text.toString()))
        adapter.notifyDataSetChanged()
    }
}