package gencana.com.weatherapp.view.custom

import android.content.Context
import android.content.res.TypedArray
import android.support.annotation.LayoutRes
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout

/**
 * Created by Gen Cana on 05/08/2018
 */
abstract class BaseView : FrameLayout {

    constructor(context: Context?) : this(context, null)
    constructor(context: Context?, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr){
        init(attrs)
    }

    private fun init(attrs: AttributeSet?) {
        val view = LayoutInflater.from(context).inflate(layoutView, this, true)
        initViews(view)
        if (attrs != null && styleableAttributes != null) {
            proccessAttributes(attrs)
        }
    }

    @get:LayoutRes
    protected abstract val layoutView: Int

    protected open val styleableAttributes: IntArray? = null

    protected abstract fun initViews(view: View)

    private fun proccessAttributes(attrs: AttributeSet) {
        val typedArray = context.theme.obtainStyledAttributes(
                attrs, styleableAttributes, 0, 0)
        try {
            declareAttributes(typedArray)
        } finally {
            typedArray.recycle()
        }
    }

    protected open fun declareAttributes(typedArray: TypedArray){

    }
}