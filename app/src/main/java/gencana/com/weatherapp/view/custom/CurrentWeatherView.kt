package gencana.com.weatherapp.view.custom

import android.content.Context
import android.util.AttributeSet
import android.view.View
import gencana.com.weatherapp.R
import gencana.com.weatherapp.common.extensions.getDrawableResourceByName
import gencana.com.weatherapp.common.extensions.toDegree
import gencana.com.weatherapp.common.utils.DateUtil.dateTimeConversionTimeZone
import gencana.com.weatherapp.common.utils.FORMATTED_FULL_DATE
import gencana.com.weatherapp.common.utils.FORMATTED_TIME
import gencana.com.weatherapp.domain.model.WeatherModel
import kotlinx.android.synthetic.main.view_current_weather.view.*

/**
 * Created by Gen Cana on 05/08/2018
 */
class CurrentWeatherView: BaseView, ViewHolder<WeatherModel> {

    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    override val layoutView: Int
        get() = R.layout.view_current_weather

    override fun initViews(view: View) {

    }

    override fun onBind(weatherModel: WeatherModel?){
        setTextValue(txt_city, weatherModel?.cityName)
        setTextValue(txt_time, weatherModel?.observationTime,
                weatherModel?.observationTime?.let { dateTimeConversionTimeZone(it, FORMATTED_FULL_DATE) })
        setTextValue(txt_date, weatherModel?.observationTime, String.format(context.getString(R.string.time_format),
                weatherModel?.observationTime?.let {dateTimeConversionTimeZone(it, FORMATTED_TIME)}))
        setTextValue(txt_temperature, weatherModel?.temp?.toDegree())
        setTextValue(txt_weather_status, weatherModel?.weatherDescription)
        setImageResourceValue(img_weather, weatherModel?.weatherIcon, context.getDrawableResourceByName(weatherModel?.weatherIcon))
    }
}