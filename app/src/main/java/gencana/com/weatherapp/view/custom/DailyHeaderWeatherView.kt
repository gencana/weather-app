package gencana.com.weatherapp.view.custom

import android.content.Context
import android.util.AttributeSet
import android.view.View
import gencana.com.weatherapp.R
import gencana.com.weatherapp.common.constant.Constant
import gencana.com.weatherapp.common.extensions.getDrawableResourceByName
import gencana.com.weatherapp.common.extensions.toDegree
import gencana.com.weatherapp.common.extensions.toPercent
import gencana.com.weatherapp.common.extensions.visible
import gencana.com.weatherapp.common.utils.*
import gencana.com.weatherapp.common.utils.DateUtil.convertDateToDay
import gencana.com.weatherapp.domain.model.WeatherDaily
import kotlinx.android.synthetic.main.view_current_weather.view.*
import java.util.*

/**
 * Created by Gen Cana on 05/08/2018
 */
class DailyHeaderWeatherView: BaseView, ViewHolder<WeatherDaily> {

    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    override val layoutView: Int
        get() = R.layout.view_current_weather

    override fun initViews(view: View) {

    }

    override fun onBind(weatherModel: WeatherDaily?){
        setTextValue(txt_city, weatherModel?.baseCityName)
        setTextValue(txt_date, weatherModel?.sunrise,
                weatherModel?.sunrise?.let { convertDateToDay(it, FORMAT_DATE_DAILY) })
        setTextValue(txt_time, String.format("%.1f%s/%.1f%s", weatherModel?.maximumTemperature,
                Constant.DEGREE_SYMBOL, weatherModel?.minimumTemperature, Constant.DEGREE_SYMBOL))
        setTextValue(txt_temperature, weatherModel?.temperature?.toDegree())
        setTextValue(txt_weather_status, weatherModel?.weatherDescription)
        setImageResourceValue(img_weather, weatherModel?.weatherIcon,
                context.getDrawableResourceByName(weatherModel?.weatherIcon))
    }


}