package gencana.com.weatherapp.view.custom

import android.content.Context
import android.util.AttributeSet
import android.widget.LinearLayout
import gencana.com.weatherapp.R
import gencana.com.weatherapp.common.extensions.getDrawableResourceByName
import gencana.com.weatherapp.common.extensions.inflateLayoutView
import gencana.com.weatherapp.common.utils.DateUtil.convertDateToDay
import gencana.com.weatherapp.domain.model.WeatherDaily
import kotlinx.android.synthetic.main.item_daily_view.view.*

/**
 * Created by Gen Cana on 07/08/2018
 */
class DailyWeatherItemView: LinearLayout, ViewHolder<WeatherDaily>  {

    private var weatherModel: WeatherDaily? = null

    constructor(context: Context?) : this(context, null)
    constructor(context: Context?, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr){
        init()
    }

    private fun init(){
        context.inflateLayoutView(R.layout.item_daily_view, this)

    }

    override fun onBind(weatherModel: WeatherDaily?){
        this.weatherModel = weatherModel
        setTextValue(txt_day, weatherModel?.timeStamp, weatherModel?.timeStamp?.let { convertDateToDay(it) })
        setTextValue(txt_max_temp, String.format(context.getString(R.string.temp_format), weatherModel?.maximumTemperature) )
        setTextValue(txt_min_temp, String.format(context.getString(R.string.temp_format), weatherModel?.minimumTemperature))
        setImageResourceValue(img_icon, weatherModel?.weatherIcon,
                context.getDrawableResourceByName(weatherModel?.weatherIcon))
    }

}