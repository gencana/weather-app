package gencana.com.weatherapp.view.custom

import android.content.Context
import android.util.AttributeSet
import android.widget.ScrollView
import gencana.com.weatherapp.R
import gencana.com.weatherapp.common.extensions.inflateLayoutView
import gencana.com.weatherapp.domain.model.WeatherDaily
import kotlinx.android.synthetic.main.view_weather_daily.view.*

/**
 * Created by Gen Cana on 07/08/2018
 */
class DailyWeatherView: ScrollView, ViewHolder<List<WeatherDaily>>{

    constructor(context: Context?) : this(context, null)
    constructor(context: Context?, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr){
        init()
    }

    private fun init(){
        context.inflateLayoutView(R.layout.view_weather_daily, this)

    }

    override fun onBind(list: List<WeatherDaily>?){
        if (layout_content.childCount > 0){
            layout_content.removeAllViews()
        }

        if (list != null) {
            for(item in list){
                var hourlyView = DailyWeatherItemView(context)
                hourlyView.onBind(item)
                layout_content.addView(hourlyView)
            }
        }
    }

  }