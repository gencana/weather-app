package gencana.com.weatherapp.view.custom

import android.content.Context
import android.util.AttributeSet
import android.view.View
import gencana.com.weatherapp.R

/**
 * Created by Gen Cana on 13/08/2018
 */
class DividerView : BaseView, ViewHolder<Nothing> {

    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)


    override val layoutView: Int
        get() = R.layout.view_divider

    override fun initViews(view: View) {

    }

    override fun onBind(data: Nothing?){

    }
}