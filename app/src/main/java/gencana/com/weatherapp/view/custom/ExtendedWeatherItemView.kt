package gencana.com.weatherapp.view.custom

import android.content.Context
import android.util.AttributeSet
import android.view.View
import gencana.com.weatherapp.R
import gencana.com.weatherapp.common.extensions.invisible
import gencana.com.weatherapp.domain.model.WeatherExtendedModel
import kotlinx.android.synthetic.main.item_extended_view.view.*

/**
 * Created by Gen Cana on 07/08/2018
 */
class ExtendedWeatherItemView : BaseView, ViewHolder<WeatherExtendedModel> {

    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)


    override val layoutView: Int
        get() = R.layout.item_extended_view

    override fun initViews(view: View) {

    }

    override fun onBind(data: WeatherExtendedModel?){
        setTextValue(txt_label_left, data?.textLabelLeft)
        setTextValue(txt_label_right, if (data?.textDetailRight.isNullOrEmpty()) "" else data?.textLabelRight)
        setTextValue(txt_detail_left, data?.textDetailLeft)
        setTextValue(txt_detail_right, data?.textDetailRight ?: "")
        data?.showDivider?.let { setInvisibilityValue(divider, !it) }
    }
}