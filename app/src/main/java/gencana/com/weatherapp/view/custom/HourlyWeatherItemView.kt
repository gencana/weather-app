package gencana.com.weatherapp.view.custom

import android.content.Context
import android.util.AttributeSet
import android.widget.LinearLayout
import gencana.com.weatherapp.R
import gencana.com.weatherapp.common.extensions.getDrawableResourceByName
import gencana.com.weatherapp.common.extensions.inflateLayoutView
import gencana.com.weatherapp.common.utils.DateUtil.convertDateToTime
import gencana.com.weatherapp.domain.model.WeatherHourly
import kotlinx.android.synthetic.main.item_hourly_view.view.*

/**
 * Created by Gen Cana on 05/08/2018
 */
class HourlyWeatherItemView: LinearLayout {

    constructor(context: Context?) : this(context, null)
    constructor(context: Context?, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr){
        init()
    }

    private fun init(){
        context.inflateLayoutView(R.layout.item_hourly_view, this)

    }

    fun onBind(weatherModel: WeatherHourly){
        if (txt_hour.tag != weatherModel.time) {
            txt_hour.text = weatherModel.time?.let { convertDateToTime(it, false) }
            txt_hour.tag = weatherModel.time
        }

        if (txt_temp.tag != weatherModel.temperature) {
            txt_temp.text = String.format(context.getString(R.string.temp_format), weatherModel.temperature)
            txt_temp.tag = weatherModel.temperature
        }

        if (txt_cloud.tag != weatherModel.clouds) {
            txt_cloud.text = String.format("%d%s", weatherModel.clouds, "%")
            txt_cloud.tag = weatherModel.clouds
        }

        if (img_icon.tag != weatherModel.weatherIcon) {
            img_icon.setImageResource(context.getDrawableResourceByName(weatherModel.weatherIcon))
            img_icon.tag = weatherModel.weatherIcon
        }
    }
}