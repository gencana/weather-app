package gencana.com.weatherapp.view.custom

import android.content.Context
import android.util.AttributeSet
import android.widget.HorizontalScrollView
import gencana.com.weatherapp.R
import gencana.com.weatherapp.common.constant.Constant.MAX_HOUR_DATA_SIZE
import gencana.com.weatherapp.common.extensions.inflateLayoutView
import gencana.com.weatherapp.domain.model.WeatherHourly
import kotlinx.android.synthetic.main.view_weather_hourly.view.*

/**
 * Created by Gen Cana on 06/08/2018
 */
class HourlyWeatherView: HorizontalScrollView, ViewHolder<List<WeatherHourly>>{

    private var hourlyListView = ArrayList<HourlyWeatherItemView>()

    constructor(context: Context?) : this(context, null)
    constructor(context: Context?, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr){
        init()
    }

    private fun init(){
        context.inflateLayoutView(R.layout.view_weather_hourly, this)
        for(i in 0 until MAX_HOUR_DATA_SIZE){
            var hourlyView = HourlyWeatherItemView(context)
            hourlyListView.add(hourlyView)
            layout_hourly.addView(hourlyView)
        }
    }

    override fun onBind(list: List<WeatherHourly>?){
        if (list != null && list.isNotEmpty()) {
            for(i in 0.. list.lastIndex){
                hourlyListView[i].onBind(list[i])
            }
        }
    }

  }