package gencana.com.weatherapp.view.custom

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import gencana.com.weatherapp.common.extensions.invisible

/**
 * Created by Gen Cana on 12/08/2018
 */
interface ViewHolder<T: Any>{

    fun onBind(data: T?)

    fun setTextValue(textView: TextView, tag: Any?, value: String? = tag.toString()){
        if (textView.tag != tag){
            textView.text = value
            textView.tag = tag
        }
    }

    fun setImageResourceValue(imageView: ImageView, tag: String?, drawable: Int){
        if (imageView.tag != tag){
            imageView.setImageResource(drawable)
            imageView.tag = tag
        }
    }

    fun setInvisibilityValue(view: View, isInvisible: Boolean){
        if (view.tag != isInvisible){
            view.invisible(isInvisible)
            view.tag = isInvisible
        }
    }
}