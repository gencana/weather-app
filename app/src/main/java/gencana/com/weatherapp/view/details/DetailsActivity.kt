package gencana.com.weatherapp.view.details


import android.app.Activity
import android.content.Intent
import android.os.Bundle
import gencana.com.weatherapp.R
import gencana.com.weatherapp.common.base.BaseActivity
import gencana.com.weatherapp.common.constant.Constant
import gencana.com.weatherapp.common.constant.Constant.KEY_DAILY_DATA
import gencana.com.weatherapp.common.constant.Constant.KEY_HOURLY_DATA
import gencana.com.weatherapp.common.utils.convertToDailyWeatherList
import gencana.com.weatherapp.domain.model.WeatherDaily
import gencana.com.weatherapp.domain.model.WeatherHourly
import gencana.com.weatherapp.view.main.MainListAdapter
import gencana.com.weatherapp.view.main.WeatherListDataModel
import kotlinx.android.synthetic.main.activity_details.*
import kotlin.reflect.KClass

/**
 * Created by Gen Cana on 13/08/2018
 */
class DetailsActivity: BaseActivity<Nothing, List<WeatherListDataModel>>() {

    companion object {
        fun startActivity(activity: Activity, weatherDaily: WeatherDaily, hourlyListData: ArrayList<WeatherHourly>?){
            var intent = Intent(activity, DetailsActivity::class.java)
            intent.putExtra(KEY_DAILY_DATA, weatherDaily)
            intent.putExtra(KEY_HOURLY_DATA, hourlyListData)
            activity.startActivity(intent)
        }
    }

    private lateinit var mainListAdapter: MainListAdapter

    private var weatherItemList: MutableList<WeatherListDataModel> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        showBackButton()
        setupList()
    }

    private fun setupList(){
        weatherItemList.clear()
        var weatherDaily: WeatherDaily = intent.getSerializableExtra(Constant.KEY_DAILY_DATA) as WeatherDaily
        var hourlyData: ArrayList<WeatherHourly> = intent.extras.get(Constant.KEY_HOURLY_DATA) as ArrayList<WeatherHourly>

        weatherItemList.add(WeatherListDataModel(Constant.VIEW_DAILY_HEADER_WEATHER, weatherDaily))
        weatherItemList.add(WeatherListDataModel(Constant.VIEW_DIVIDER))

        if (!hourlyData.isEmpty()){
            weatherItemList.add(WeatherListDataModel(Constant.VIEW_HOURLY_WEATHER, hourlyData))
            weatherItemList.add(WeatherListDataModel(Constant.VIEW_DIVIDER))
        }

        weatherItemList.addAll(convertToDailyWeatherList(Constant.VIEW_EXTENDED_WEATHER, weatherDaily))
        weatherItemList.add(WeatherListDataModel(Constant.VIEW_DIVIDER))
        mainListAdapter = MainListAdapter(this, weatherItemList)
        list_view.adapter = mainListAdapter
    }

    override fun getLayout(): Int {
        return R.layout.activity_details
    }

    override fun getViewModel(): KClass<Nothing>? {
        return null
    }

    override fun onResponseFailed(message: String?) {
    }

    override fun onResponseSuccess(data: List<WeatherListDataModel>) {

    }

}