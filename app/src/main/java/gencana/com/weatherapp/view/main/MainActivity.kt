package gencana.com.weatherapp.view.main

import android.app.Activity
import android.content.Intent
import android.location.Location
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import gencana.com.weatherapp.R
import gencana.com.weatherapp.common.base.BaseActivity
import gencana.com.weatherapp.common.extensions.toast
import gencana.com.weatherapp.common.extensions.visible
import gencana.com.weatherapp.domain.model.WeatherDaily
import gencana.com.weatherapp.view.details.DetailsActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.reflect.KClass
import gencana.com.weatherapp.common.utils.*
import kotlin.collections.ArrayList
import gencana.com.weatherapp.common.constant.Constant
import gencana.com.weatherapp.common.utils.DateUtil.convertDateToDay
import gencana.com.weatherapp.view.changelocation.ChangeLocationActivity
import kotlinx.android.synthetic.main.error_view.*


class MainActivity : BaseActivity<MainViewModel, List<WeatherListDataModel>>(),
        MainListAdapter.ItemListener, LocationHelper.LocationListener{

    private var weatherItemList: MutableList<WeatherListDataModel>? = ArrayList()

    private lateinit var mainListAdapter: MainListAdapter

    private var locationHelper: LocationHelper? = null

    private var locationMenuItem: MenuItem? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setup()
    }

    private fun setup(){
        mainListAdapter = MainListAdapter(this, weatherItemList, this)
        list_view.adapter = mainListAdapter

        locationHelper = LocationHelper(this, this)

        btn_retry.setOnClickListener{
            if (viewModel?.location == null && viewModel?.city == null){
                showAutoCompleteCity()
            }else{
                viewModel?.execute()
            }

        }
    }

    private fun enableCurrentLocationMenu(isEnable: Boolean){
        locationMenuItem?.isEnabled = isEnable
    }

    override fun onResponseFailed(message: String?) {
        enableCurrentLocationMenu(true)
        showErrorView()
        toast(message ?: getString(R.string.error_unknown))
    }

    override fun onResponseSuccess(data: List<WeatherListDataModel>) {
        enableCurrentLocationMenu(true)
        showContent()
        weatherItemList?.addAll(data)
        mainListAdapter.notifyDataSetChanged()
    }

    override fun getLayout(): Int {
        return R.layout.activity_main
    }

    override fun getViewModel(): KClass<MainViewModel> {
        return MainViewModel::class
    }

    override fun onItemClicked(data: WeatherListDataModel) {
        var weatherDaily = data.data as WeatherDaily
        DetailsActivity.startActivity(this, weatherDaily,
                weatherDaily.sunrise?.let { convertDateToDay(it, FORMAT_DATE) }?.let {
                    viewModel?.getHourlyWeatherData(it)
                })
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        locationHelper?.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onLocationPermissionDenied() {
        onLocationFailed()
    }

    override fun onLocationSuccess(location: Location) {
        viewModel?.city = null
        viewModel?.location = location
        weatherItemList?.clear()
        mainListAdapter.notifyDataSetChanged()
        viewModel?.execute()
    }

    override fun onLocationFailed() {
        showAutoCompleteCity()
        toast(R.string.location_access_failed)
    }

    private fun showContent(){
        list_view.visible()
        view_error.visible(false)
    }

    override fun showLoading(show: Boolean) {
        super.showLoading(show)
        if (show){
            view_error.visible(false)
        }
    }

    private fun showAutoCompleteCity(){
        ChangeLocationActivity.startActivityForResult(this)
    }

    override fun showErrorView(show: Boolean){
        list_view.visible(!show)
        view_error.visible(show)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        super.onOptionsItemSelected(item)
        when(item.itemId){
            R.id.action_change_location -> showAutoCompleteCity()
            R.id.action_current_location -> {
                locationMenuItem = item
                enableCurrentLocationMenu(false)
                toast(getString(R.string.retrieving_location))
                locationHelper?.setupLocation()
            }
        }
        return true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when(resultCode){
            Activity.RESULT_OK -> searchCity(data?.getStringExtra(Constant.KEY_CITY)!!)
            else -> {
                if (weatherItemList?.size == 0){
                    showErrorView()
                }
            }
        }
    }

    private fun searchCity(city: String){
        viewModel?.location = null
        viewModel?.city = city
        weatherItemList?.clear()
        mainListAdapter.notifyDataSetChanged()
        viewModel?.execute()
    }
}
