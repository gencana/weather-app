package gencana.com.weatherapp.view.main

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import gencana.com.weatherapp.common.constant.Constant
import gencana.com.weatherapp.view.custom.ViewHolder


/**
 * Created by Gen Cana on 12/08/2018
 */
class MainListAdapter
constructor(
        private  var ctx: Context,
        private var list: MutableList<WeatherListDataModel>?,
        private var itemListener: ItemListener? = null
): ArrayAdapter<WeatherListDataModel>(ctx, 0, list) {

    interface ItemListener{

        fun onItemClicked(data: WeatherListDataModel)
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var v = convertView
        var data = getItem(position)
        var type = data.viewHolder
        var viewHolder: ViewHolder<Any> ?= null

        //create viewholder if null or get from tag
        if (v == null) {
            v = ViewHolderFactory.create(context, type!!)
            viewHolder = v as ViewHolder<Any>
            v.tag = viewHolder
        } else {
            viewHolder = v.tag as ViewHolder<Any>
        }

        if (type == Constant.VIEW_DAILY_WEATHER){
            v.setOnClickListener{
                itemListener?.onItemClicked(getItem(position))
            }
        }
        
        viewHolder.onBind(data.data)
        return v
    }

    override fun getCount(): Int {
        return list?.size ?: 0
    }

    override fun getItem(position: Int): WeatherListDataModel {
        return list?.get(position)!!
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getViewTypeCount(): Int {
        return Constant.VIEW_TYPE_COUNT
    }

    override fun getItemViewType(position: Int): Int {
        return getItem(position).viewHolder ?: 0
    }
}