package gencana.com.weatherapp.view.main

import android.location.Location
import gencana.com.weatherapp.common.base.BaseViewModel
import gencana.com.weatherapp.common.constant.ApiConstants
import gencana.com.weatherapp.common.constant.Constant
import gencana.com.weatherapp.common.constant.Constant.MAX_HOUR_DATA_SIZE
import gencana.com.weatherapp.common.utils.*
import gencana.com.weatherapp.common.utils.DateUtil.setupTimeZone
import gencana.com.weatherapp.data.remote.http.ApiResponse
import gencana.com.weatherapp.data.remote.http.HttpGetCallTask
import gencana.com.weatherapp.domain.model.WeatherDaily
import gencana.com.weatherapp.domain.model.WeatherHourly
import gencana.com.weatherapp.domain.model.WeatherModel
import gencana.com.weatherapp.domain.repository.WeatherRepository
import java.util.*
import kotlin.collections.ArrayList

/**
 * Created by Gen Cana on 05/08/2018
 */
class MainViewModel: BaseViewModel<List<WeatherListDataModel>>() {

    var location: Location? = null
    var city: String? = null

    private var itemHeaderList = ArrayList<WeatherListDataModel>()
    private var viewHolderSet = HashSet<Int>()
    private var hourlyWeatherListData = ArrayList<WeatherHourly>()

    override fun execute() {
        loadingLiveData.postValue(true)
        itemHeaderList.clear()
        viewHolderSet.clear()
        hourlyWeatherListData.clear()
        getCurrentWeather()
        getDailyWeather()
        getHourlyWeather()
    }


    private fun getCurrentWeather(){
        getCurrentWeatherParams(location, city)?.let {
            WeatherRepository.getCurrentWeather(it,
                object: HttpGetCallTask.ResponseCallback<WeatherModel>{
                    override fun onResultFailed(message: String) {
                        loadingLiveData.postValue(false)
                        errorLiveData.postValue(getErrorMessage(message))
                    }

                    override fun onResultSuccess(response: ApiResponse<WeatherModel>) {
                        setupTimeZone(response.data?.first()?.timezone)
                        var viewHolder = Constant.VIEW_CURRENT_WEATHER
                        itemHeaderList.add(0, WeatherListDataModel(viewHolder, response.data?.first()))
                        viewHolderSet.add(viewHolder)

                        viewHolder = Constant.VIEW_EXTENDED_WEATHER
                        response.data?.flatMap {
                            convertToExtendedWeatherList(viewHolder, response.data?.first())
                        }?.let {
                            itemHeaderList.addAll(it)
                        }
                        viewHolderSet.add(viewHolder)
                        updateData()
                    }
                })
        }
    }

    private fun getHourlyWeather(){
        getCurrentWeatherParams(location, city)?.let {
            WeatherRepository.getHourlyWeather(it, object: HttpGetCallTask.ResponseCallback<WeatherHourly>{
            override fun onResultFailed(message: String) {
                loadingLiveData.postValue(false)
                errorLiveData.postValue(getErrorMessage(message))
            }

            override fun onResultSuccess(response: ApiResponse<WeatherHourly>) {
                setupTimeZone(response.data?.first()?.baseTimeZone)
                var viewHolder = Constant.VIEW_HOURLY_WEATHER
                hourlyWeatherListData = response.data as ArrayList<WeatherHourly>
                itemHeaderList.add(WeatherListDataModel(viewHolder, response.data?.take(MAX_HOUR_DATA_SIZE)))
                viewHolderSet.add(viewHolder)
                updateData()
            }

        })
        }
    }


    private fun updateData(){
        if (viewHolderSet.size == 4){
            itemHeaderList.sortBy {
                it.viewHolder
            }
            var viewType: Int? = null
            for (index in itemHeaderList.lastIndex downTo 0){
                var item = itemHeaderList.get(index)
                if (item.viewHolder != viewType){
                    itemHeaderList.add(index+1, WeatherListDataModel(Constant.VIEW_DIVIDER))
                    viewType = item.viewHolder
                }
            }
            loadingLiveData.postValue(false)
            responseLiveData.postValue(itemHeaderList)
        }
    }

    private fun getDailyWeather(){
        getDailyWeatherParams(location, city)?.let { it ->
            WeatherRepository.getDailyWeather(it, object: HttpGetCallTask.ResponseCallback<WeatherDaily>{
            override fun onResultFailed(message: String) {
                loadingLiveData.postValue(false)
                errorLiveData.postValue(getErrorMessage(message))
            }

            override fun onResultSuccess(response: ApiResponse<WeatherDaily>) {
                setupTimeZone(response.data?.first()?.baseTimeZone)
                var viewHolder = Constant.VIEW_DAILY_WEATHER
                response.data?.map {
                    WeatherListDataModel(viewHolder, it)}?.let { itemHeaderList.addAll(it)
                }
                viewHolderSet.add(viewHolder)
                updateData()
            }

        })
        }
    }

    fun getHourlyWeatherData(date: String): ArrayList<WeatherHourly>{
        var hourlyWeatherList = ArrayList<WeatherHourly>()
        for (item in hourlyWeatherListData){
            if (item.time?.indexOf("T")?.let { item.time?.substring(0, it) } == date) {
                hourlyWeatherList.add(item)
            }
        }

        return if (hourlyWeatherList.size == MAX_HOUR_DATA_SIZE) hourlyWeatherList else ArrayList()
    }

    private fun getErrorMessage(message: String): String{
        if (city != null && message == ApiConstants.ERROR_CODE_NO_CONTENT.toString()){
            showErrorViewLiveData.postValue(true)
            return ApiConstants.ERROR_LOCATION
        }

        return message
    }

}