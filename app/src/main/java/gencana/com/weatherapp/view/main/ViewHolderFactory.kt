package gencana.com.weatherapp.view.main

import android.content.Context
import android.view.View
import gencana.com.weatherapp.common.constant.Constant
import gencana.com.weatherapp.view.custom.*

/**
 * Created by Gen Cana on 12/08/2018
 */

//factory for creating viewholder for multi listview items
object ViewHolderFactory {
    fun create(context: Context, viewType: Int): View?{
        var view: View? = null
        view =
                when (viewType) {
                    Constant.VIEW_CURRENT_WEATHER -> CurrentWeatherView(context)
                    Constant.VIEW_DAILY_WEATHER -> DailyWeatherItemView(context)
                    Constant.VIEW_HOURLY_WEATHER -> HourlyWeatherView(context)
                    Constant.VIEW_EXTENDED_WEATHER -> ExtendedWeatherItemView(context)
                    Constant.VIEW_DIVIDER -> DividerView(context)
                    Constant.VIEW_DAILY_HEADER_WEATHER -> DailyHeaderWeatherView(context)
                    else -> {
                        null
                    }
                }


        return view
    }

}