package gencana.com.weatherapp.view.main

/**
 * Created by Gen Cana on 12/08/2018
 */
data class WeatherListDataModel(
        val viewHolder: Int?= null,
        var data: Any? = null,
        var isSelected: Boolean = false
)