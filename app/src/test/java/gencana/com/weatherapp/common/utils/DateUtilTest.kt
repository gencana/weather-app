package gencana.com.weatherapp.common.utils

import org.junit.Test

import org.junit.Assert.*

/**
 * Created by Gen Cana on 27/08/2018
 */
class DateUtilTest {

    @Test
    fun setupTimeZone() {
    }

    @Test
    fun convertStringToDate() {
    }

    @Test
    fun formatDate() {
    }

    @Test
    fun convertDateToTime() {
    }

    @Test
    fun convertDateToDay() {
    }

    @Test
    fun formatCurrentDate() {
    }

    @Test
    fun concatenatedDateTimeConversionTimeZone() {
    }

    @Test
    fun dateTimeConversionTimeZone() {
    }
}